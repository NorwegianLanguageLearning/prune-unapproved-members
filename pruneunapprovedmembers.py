#!/usr/bin/env python3
import discord
import datetime
import logging

from secrets import token

guild_id = 143_458_761_665_675_264
user_role_id = 476_133_390_664_335_361
max_days_since_joining = 7


async def prune_unapproved_members(client):
    await client.wait_until_ready()
    guild = client.get_guild(guild_id)
    if guild is None:
        print("Guild not found.")
        return await client.logout()
    user_role = guild.get_role(user_role_id)
    if user_role is None:
        print("User role not found.")
        return await client.logout()
    for member in guild.members:
        if (
            member.joined_at
            < datetime.datetime.now() - datetime.timedelta(days=max_days_since_joining)
            and user_role not in member.roles
        ):
            print(f"Kicking {member.name}")
            await member.kick(
                reason=f"Stayed in the server for at least {max_days_since_joining} days without gaining the User role."
            )
    await client.logout()


client = discord.Client()

client.loop.create_task(prune_unapproved_members(client))
client.run(token)
